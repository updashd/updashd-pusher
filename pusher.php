<?php

use Ratchet\Server\IoServer;

chdir(__DIR__);

require 'vendor/autoload.php';

set_error_handler('errorHandler');

function errorHandler($severity, $message, $filename, $lineNo) {
    if (error_reporting() == 0) {
        return;
    }

    if (error_reporting() & $severity) {
        throw new ErrorException($message, 0, $severity, $filename, $lineNo);
    }
}

$config = include 'config/config.default.php';

$env = getenv('ENVIRONMENT') ? : 'local';
$envConfigFileName = 'config/config.' . $env . '.php';

if (file_exists($envConfigFileName)) {
    $envConfig = include $envConfigFileName;

    $config = array_replace_recursive($config, $envConfig);
}

// Create Pusher Application
$pusher = new \Updashd\Pusher\Pusher($config);

echo 'Creating IoServer...' . PHP_EOL;

$server = IoServer::factory(
    new Ratchet\Http\HttpServer(
        new Ratchet\WebSocket\WsServer(
            $pusher
        )
    ),
    8080
);

$pusher->redisInit($server->loop);

echo 'Running event loop...' . PHP_EOL;
$server->run();
