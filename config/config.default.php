<?php
$redisAddr = getenv('REDIS_ADDR') ?: '127.0.0.1';
$redisPort = getenv('REDIS_PORT') ?: 6379;
$redisPassword = getenv('REDIS_PASS') ?: null;

$redisParams = [];

if ($redisPassword) {
    $redisParams['password'] = $redisPassword;
}

$redisUri = 'redis://' . $redisAddr . ':' . $redisPort . ($redisParams ? '?' . http_build_query($redisParams) : '');

return [
    'redis' => $redisUri,
    'jwt' => [
        'algorithm' => 'HS256',
        'key' => getenv('PUSHER_JWT_KEY') ?: 'abc123',
    ]
];