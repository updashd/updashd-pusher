<?php
namespace Updashd\Pusher;

use Firebase\JWT\JWT;
use Predis\ClientInterface;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Clue\React\Redis\Client as RedisClient;
use Clue\React\Redis\Factory as RedisFactory;
use React\EventLoop\Factory as ReactFactory;
use React\EventLoop\LoopInterface;
use Updashd\Scheduler\Model\Result;
use Updashd\Scheduler\Model\Task;
use Updashd\Scheduler\Scheduler;

class Pusher implements MessageComponentInterface {

    const CLIENTS = 'clients';
    const CONTEXT = 'context';
    const NSZID = 'nszid';

    /** @var \SplObjectStorage */
    protected $connections;

    /** @var array Key is nszid, Value is array of hashes*/
    protected $channelSubscriptions = [];

    /** @var array Array of callables. Key is pattern. Value is callable. */
    protected $psubscriptions = [];

    /** @var array */
    protected $config;

    /** @var RedisClient */
    protected $subscriptionClient;

    /** @var RedisClient */
    protected $getClient;

    public function __construct ($config) {
        $this->connections = new \SplObjectStorage();
        $this->config = $config;
    }

    protected function getChannelName ($nszid) {
        return Scheduler::CHANNEL_STATE_CHANGE_PREFIX . Task::getTaskKeyName(null, $nszid);
    }

    public function redisInit (LoopInterface $loop) {
        $factory = new RedisFactory($loop);

        $factory->createClient($this->config['redis'])->then(function (RedisClient $client) {
            $this->getClient = $client;

            echo 'Get client connected';
        }, function ($e) {
            echo 'Get connect failed! ' . $e;
        });

        $factory->createClient($this->config['redis'])->then(function (RedisClient $client) {
            $this->subscriptionClient = $client;

            echo 'Sending psubscribe...' . PHP_EOL;

            $this->psubscribe($client, Scheduler::CHANNEL_STATE_CHANGE_PREFIX . '*', [$this, 'onStatusUpdateFromRedis']);
//            $this->psubscribe($client, Scheduler::CHANNEL_RESULT_PREFIX . '*', [$this, 'onResultUpdateFromRedis']);

            $client->on('pmessage', function ($pattern, $channel, $payload) {
                if (array_key_exists($pattern, $this->psubscriptions)) {
                    $this->psubscriptions[$pattern]($channel, $payload);
                }
                else {
                    echo 'Cannot handle pmessage. Unknown pattern!' . PHP_EOL;
                }
            });
        }, function ($e) {
            echo 'Subscription connect failed! ' . $e;
        });
    }

    public function psubscribe(RedisClient $client, string $pattern, callable $callback) {
        $this->psubscriptions[$pattern] = $callback;

        $client->psubscribe($pattern)->then(function () use ($pattern) {
            echo 'Now subscribed to pattern ' . $pattern . PHP_EOL;
        }, function ($e) {
            echo 'Subscription failed!' . PHP_EOL;
        });
    }

    public function addChannelSubscription ($nszid, $channel, $connection) {
        echo 'Client Subscription: ' . $channel . PHP_EOL;
        // Subscribe if needed
        if (! array_key_exists($channel, $this->channelSubscriptions)) {
            $this->channelSubscriptions[$channel] = [
                self::CLIENTS => [],
                self::NSZID => $nszid
            ];
        }

        $this->channelSubscriptions[$channel][self::CLIENTS][] = $connection;
    }

    public function removeChannelSubscription ($channel, $connection) {
        echo 'Client Unsubscription: ' . $channel . PHP_EOL;

        // Subscribe if needed
        if (array_key_exists($channel, $this->channelSubscriptions)) {
            $subProps = $this->channelSubscriptions[$channel];

            // Search for the connection
            $idx = array_search($connection, $subProps[self::CLIENTS]);

            // If we found the connection
            if ($idx !== false) {
                // Remove it
                unset($this->channelSubscriptions[$channel][self::CLIENTS][$idx]);
            }
        }

    }

    public function getChannelOptions ($channel, $field = self::CLIENTS) {
        // Subscribe if needed
        if (array_key_exists($channel, $this->channelSubscriptions)) {
            $subProps = $this->channelSubscriptions[$channel];
            return $subProps[$field];
        }
        else {
            return null;
        }
    }

    /**
     * @param $channel
     * @return ConnectionInterface[]
     */
    public function getChannelSubscriptions ($channel) {
        return $this->getChannelOptions($channel, self::CLIENTS) ?: [];
    }

    /**
     * @param $channel
     * @return ConnectionInterface[]
     */
    public function getChannelNszid ($channel) {
        return $this->getChannelOptions($channel, self::NSZID);
    }

    public function removeConnectionSubscriptions ($connection) {
        foreach ($this->channelSubscriptions as $channel => $subProps) {
            $this->removeChannelSubscription($channel, $connection);
        }
    }

    /**
     * When a new connection is opened it will be passed to this method
     * @param  ConnectionInterface $conn The socket/connection that just connected to your application
     * @throws \Exception
     */
    public function onOpen (ConnectionInterface $conn) {
        // Add it to the object store
        $this->connections->attach($conn);

        echo 'Connection: ' . $conn->resourceId  . PHP_EOL;
    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    public function onClose (ConnectionInterface $conn) {
        // Remove it from the object store
        $this->connections->detach($conn);

        $this->removeConnectionSubscriptions($conn);

        echo 'Disconnect: ' . $conn->resourceId  . PHP_EOL;
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    public function onError (ConnectionInterface $conn, \Exception $e) {
        echo 'Error occurred!.' . PHP_EOL;
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    public function onMessage (ConnectionInterface $from, $msg) {
        try {
            $msgObj = json_decode($msg, true);

            $decoded = JWT::decode($msgObj['jwt'], $this->config['jwt']['key'], [$this->config['jwt']['algorithm']]);

            $nszid = $decoded->nszid;

            $channel = Scheduler::CHANNEL_STATE_CHANGE_PREFIX . Task::getTaskKeyName(null, $nszid);
            $this->addChannelSubscription($nszid, $channel, $from);
        }
        catch (\Exception $e) {
            echo 'Got Invalid message. Error: ' . $e->getMessage() . PHP_EOL;
        }

    }

    public function onStatusUpdateFromRedis($channel, $msg) {
        $subscriptions = $this->getChannelSubscriptions($channel);
        $nszid = $this->getChannelNszid($channel);

        foreach ($subscriptions as $connection) {
            $connection->send(json_encode([
                'nszid' => $nszid,
                'state' => $msg
            ]));
        }
    }

    public function onResultUpdateFromRedis($channel, $msg) {
        $subscriptions = $this->getChannelSubscriptions($channel);
        $nszid = $this->getChannelNszid($channel);

        echo $channel . ' ' . $msg . PHP_EOL;

        $resultKey = Result::NAME . ':' . $msg;

        $result = new Result(new FakeClient(), null, $msg);
        $fields = $result->getFields();

        $args = array_merge([$resultKey], $fields);

        call_user_func_array([$this->getClient, 'hmget'], $args)->then(function ($values) use ($result, $fields) {
            $result->populateFields($fields, $values);
        }, function ($e) {
            echo $e->getMessage() . PHP_EOL;
        });
    }
}